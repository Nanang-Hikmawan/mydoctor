import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  ImageBackground,
} from 'react-native';
import React from 'react';
import {colors} from '../../utils/colors';
import {fonts} from '../../utils/fonts';
import {BgHospitals} from '../../assets/ilustration';
import {ListHospitals} from '../../component/compBig';
import {DummyHospital1, DummyHospital2, DummyHospital3} from '../../assets';

const Hospitals = () => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <View style={styles.content}>
        <ImageBackground source={BgHospitals} style={styles.bgImage}>
          <Text style={styles.title}>Nearby Hospitals</Text>
          <Text style={styles.title1}>3 tersedia</Text>
        </ImageBackground>
        <View style={styles.body}>
          <ListHospitals
            type={'Rumah Sakit'}
            name={'Citra Bunga Merdeka'}
            address={'Jln. Surya Sejahtera 20'}
            pict={DummyHospital1}
          />
          <ListHospitals
            type={'Rumah Sakit Anak'}
            name={'Happy Family & Kids'}
            address={'Jln. Sentoas 12'}
            pict={DummyHospital2}
          />
          <ListHospitals
            type={'Rumah Sakit Jiwa'}
            name={'Sajar Waras'}
            address={'Jln. Surya Kencana 10'}
            pict={DummyHospital3}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Hospitals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  body: {
    flex: 1,
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: -30,
    paddingTop: 14,
  },
  bgImage: {
    height: 240,
    paddingTop: 30,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.white,
    textAlign: 'center',
  },
  title1: {
    fontSize: 14,
    fontFamily: fonts.primary[300],
    color: colors.white,
    textAlign: 'center',
  },
});
