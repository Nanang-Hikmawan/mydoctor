import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors} from '../../../utils/colors';
import {fonts} from '../../../utils/fonts';

const Link = ({label, onPress, size, align}) => {
  return (
    <View>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.label(size, align)}>{label}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Link;

const styles = StyleSheet.create({
  label: (size, align) => ({
    fontFamily: fonts.primary[400],
    fontSize: size,
    color: colors.text.secondary,
    marginBottom: 40,
    textDecorationLine: 'underline',
    textAlign: align,
  }),
});
