import {StyleSheet, Text, View, SafeAreaView, StatusBar} from 'react-native';
import React from 'react';
import {colors} from '../../utils/colors';
import {ChatItem, Header, InputChat} from '../../component/compBig';
import {fonts} from '../../utils/fonts';

const Chatting = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle="light-content"
      />
      <Header
        title={'Naerobi Putri Hayaza'}
        type="dark-header"
        onPress={() => navigation.goBack()}
      />
      <View style={styles.content}>
        <Text style={styles.text}>Senin, 21 Maret, 2020</Text>
        <ChatItem isMe />
        <ChatItem />
      </View>
      <InputChat />
    </SafeAreaView>
  );
};

export default Chatting;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    flex: 1,
  },
  text: {
    fontSize: 11,
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    marginVertical: 20,
    textAlign: 'center',
  },
});
