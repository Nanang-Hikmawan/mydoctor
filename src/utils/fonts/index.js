export const fonts = {
  primary: {
    200: 'Nunito-ExtraLight',
    300: 'Nunito-Light',
    400: 'Nunito-Regular',
    600: 'Nuniot-SemiBold',
    700: 'Nunito-Bold',
    800: 'Nunito-EstraBold',
    900: 'Nunito-Black',
  },
};
