import Logo from './logo.svg';
import Background from './backgroundimage.png';
import UserImageNull from './userPhotoNull.png';
import UserPhoto from './user_photo.png';
import ObatPict from './cat-dok-obat.svg';
import DokPsikiater from './cat-dok-psikiater.svg';
import DokUmum from './cat-dok-umum.svg';
import BgHospitals from './hospitals-background.png';

export {
  Logo,
  Background,
  UserImageNull,
  UserPhoto,
  ObatPict,
  DokPsikiater,
  DokUmum,
  BgHospitals,
};
