import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
} from 'react-native';
import React from 'react';
import {colors} from '../../utils/colors';
import {
  DoctorCategory,
  HomeProfile,
  NewsItem,
  RatedDoctor,
} from '../../component/compBig';
import {fonts} from '../../utils/fonts';
import {Gap} from '../../component/compLess';
import {JSONCategoryDoctor} from '../../assets';

const Doctors = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <View style={styles.content}>
        <View style={styles.main}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.context}>
              <Gap height={40} />
              <HomeProfile />
              <Text style={styles.text}>
                Mau konsultasi dengan siapa hari ini?
              </Text>
            </View>
            <View style={styles.wrapper}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={styles.category}>
                  <Gap width={32} />
                  {JSONCategoryDoctor.data.map(item => {
                    return (
                      <DoctorCategory
                        category={item.category}
                        key={item.id}
                        onPress={() => navigation.navigate('ChoseDoctor')}
                      />
                    );
                  })}
                  <Gap width={22} />
                </View>
              </ScrollView>
            </View>
            <View style={styles.context}>
              <Text style={styles.label}>Top Rated Doctors</Text>
              <RatedDoctor />
              <RatedDoctor />
              <RatedDoctor />
              <Text style={styles.label}>Good News</Text>
            </View>
            <NewsItem />
            <NewsItem />
            <NewsItem />
            <Gap height={40} />
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Doctors;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  main: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  text: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 30,
    marginBottom: 16,
    maxWidth: 209,
  },
  wrapper: {
    marginHorizontal: -16,
  },
  category: {
    flexDirection: 'row',
  },
  label: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginBottom: 16,
    marginTop: 30,
  },
  context: {
    paddingHorizontal: 16,
  },
});
