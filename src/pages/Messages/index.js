import {StyleSheet, Text, View, SafeAreaView, StatusBar} from 'react-native';
import React, {useState} from 'react';
import {colors} from '../../utils/colors';
import {List} from '../../component/compBig';
import {fonts} from '../../utils/fonts';
import {DummyDoctor4, DummyDoctor5, DummyDoctor6} from '../../assets';

const Messages = () => {
  const [data, setData] = useState([
    {
      id: 1,
      profile: DummyDoctor4,
      name: 'Alexander Jannie',
      desc: 'Baik bu, terima kasih banyak atas waktunya',
    },
    {
      id: 2,
      profile: DummyDoctor5,
      name: 'Alexander Jennie',
      desc: 'Oh tentu saja tidak karena jeruk it...',
    },
    {
      id: 3,
      profile: DummyDoctor6,
      name: 'Nairobi Putri Hayza',
      desc: 'Oke menurut pak dokter bagaimana unt...',
    },
  ]);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          <Text style={styles.title}>Messages</Text>
          {data.map(item => {
            return (
              <List
                profile={item.profile}
                name={item.name}
                desc={item.desc}
                key={item.id}
              />
            );
          })}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Messages;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  page: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  title: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
    marginTop: 30,
    marginLeft: 16,
  },
});
