import {StyleSheet, View, SafeAreaView, StatusBar} from 'react-native';
import React from 'react';
import {Header} from '../../component/compBig';
import {Button, Gap, Input} from '../../component';
import {colors} from '../../utils/colors';

const Register = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Header title={'Daftar Akun'} onPress={() => navigation.goBack()} />
      <View style={styles.content}>
        <Gap height={40} />
        <Input label={'Nama Lengkap'} />
        <Gap height={24} />
        <Input label={'Pekerjaan'} />
        <Gap height={24} />
        <Input label={'Alamat Email'} />
        <Gap height={24} />
        <Input label={'Password'} />
        <Gap height={40} />
        <Button
          title={'Lanjut'}
          onPress={() => navigation.navigate('UplaodPhoto')}
        />
      </View>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    padding: 40,
    paddingTop: 0,
    backgroundColor: colors.white,
  },
});
