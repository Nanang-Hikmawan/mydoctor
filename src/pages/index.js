import SplashScreen from './SplashScreen';
import GetStarted from './GetStarted';
import Login from './Login';
import Register from './Register';
import UploadPhoto from './UploadPhoto';
import Doctors from './Doctors';
import Messages from './Messages';
import Hospitals from './Hospitals';
import ChoseDoctor from './ChoseDoctor';
import Chatting from './Chatting';

export {
  SplashScreen,
  GetStarted,
  Login,
  Register,
  UploadPhoto,
  Doctors,
  Messages,
  Hospitals,
  ChoseDoctor,
  Chatting,
};
