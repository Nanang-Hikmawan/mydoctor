import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap} from '../../compLess';
import {fonts} from '../../../utils/fonts';
import {colors} from '../../../utils/colors';
import DarkHeader from './DarkHeader';
import {DummyDoctor9} from '../../../assets';
import {useNavigation} from '@react-navigation/native';

const Header = ({title, onPress, type}) => {
  const navigation = useNavigation();

  if (type === 'dark-header') {
    return (
      <DarkHeader
        title={'Naerobi Putri Hayaza'}
        desc="Dokter Anak"
        photo={DummyDoctor9}
        onPress={() => navigation.goBack()}
      />
    );
  }

  return (
    <View style={styles.container(type)}>
      <Button
        type={'icon-only'}
        iconName={type === 'dark' ? 'iconBack-light' : 'iconBack-dark'}
        onPress={onPress}
      />
      <Text style={styles.title(type)}>{title}</Text>
      <Gap width={24} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: type => ({
    paddingHorizontal: 16,
    paddingTop: 40,
    paddingBottom: 30,
    backgroundColor: type === 'dark' ? colors.secondary : colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomLeftRadius: type === 'dark' ? 20 : 0,
    borderBottomRightRadius: type === 'dark' ? 20 : 0,
  }),
  title: type => ({
    flex: 1,
    textAlign: 'center',
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: type === 'dark' ? colors.white : colors.text.primary,
  }),
});
