import IconBack from './arrowback.svg';
import IconBackLight from './arrowbacklight.svg';
import ButtonAdd from './ic-add-photo.svg';
import ButtonRemove from './ic-remove-photo.svg';
import IconDoctor from './ic-doctor.svg';
import IconDoctorActive from './ic-doctor-active.svg';
import IconHospitals from './ic-hospitals.svg';
import IconHospitalsActive from './ic-hospitals-active.svg';
import IconMessages from './ic-messages.svg';
import IconMessagesActive from './ic-messages-active.svg';
import IconStar from './ic-star.svg';
import IconNext from './ic-next.svg';
import IconRate from './ic-rate.svg';
import IconMale from './ic-male.svg';
import IconHelp from './ic-help.svg';
import IconLanguage from './ic-language.svg';
import IconSendDark from './ic-send-dark.svg';
import IconSendLight from './ic-send-light.svg';
import IconEditProfile from './ic-edit-profile.svg';

export {
  IconBack,
  IconBackLight,
  ButtonAdd,
  ButtonRemove,
  IconDoctor,
  IconDoctorActive,
  IconHospitals,
  IconHospitalsActive,
  IconMessages,
  IconMessagesActive,
  IconStar,
  IconNext,
  IconRate,
  IconMale,
  IconHelp,
  IconLanguage,
  IconSendDark,
  IconSendLight,
  IconEditProfile,
};
