import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {fonts} from '../../../utils/fonts';
import {colors} from '../../../utils/colors';

const IsMe = () => {
  return (
    <View style={styles.content}>
      <View style={styles.body}>
        <Text style={styles.text}>
          Ibu dokter, apakah memakan jeruk tiap hari itu buruk?
        </Text>
      </View>
      <Text style={styles.text1}>4.20 AM</Text>
    </View>
  );
};

export default IsMe;

const styles = StyleSheet.create({
  content: {
    marginBottom: 20,
    alignItems: 'flex-end',
    paddingRight: 16,
  },
  body: {
    maxWidth: '70%',
    padding: 12,
    paddingRight: 18,
    backgroundColor: colors.cardLight,
    borderRadius: 10,
    borderBottomRightRadius: 0,
  },
  text: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
  text1: {
    fontFamily: fonts.primary[400],
    fontSize: 11,
    color: colors.secondary,
    marginTop: 8,
  },
});
