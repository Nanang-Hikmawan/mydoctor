import Header from './Header';
import CustomTabBar from './CustomTabBar';
import HomeProfile from './HomeProfile';
import DoctorCategory from './DoctorCategory';
import RatedDoctor from './RatedDoctor';
import NewsItem from './NewsItem';
import List from './List';
import ListHospitals from './ListHospitals';
import ChatItem from './ChatItem';
import InputChat from './InputChat';

export {
  Header,
  CustomTabBar,
  HomeProfile,
  DoctorCategory,
  RatedDoctor,
  NewsItem,
  List,
  ListHospitals,
  ChatItem,
  InputChat,
};
