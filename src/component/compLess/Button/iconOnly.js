import {TouchableOpacity} from 'react-native';
import React from 'react';
import {IconBack, IconBackLight} from '../../../assets';

const IconOnly = ({onPress, iconName}) => {
  const Icon = () => {
    if (iconName === 'iconBack-dark') {
      return <IconBack />;
    }
    if (iconName === 'iconBack-light') {
      return <IconBackLight />;
    }
    return <IconBack />;
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <Icon />
    </TouchableOpacity>
  );
};

export default IconOnly;
