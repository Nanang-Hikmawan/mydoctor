import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  Image,
} from 'react-native';
import React from 'react';
import {colors} from '../../utils/colors';
import {Button, Gap, Header, Link} from '../../component';
import {ButtonAdd, UserImageNull} from '../../assets';
import {fonts} from '../../utils/fonts';

const UploadPhoto = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Header title={'Upload Photo'} onPress={() => navigation.goBack()} />
      <View style={styles.content}>
        <View style={styles.header}>
          <View style={styles.main}>
            <Image source={UserImageNull} style={styles.image} />
            <ButtonAdd style={styles.btn} />
          </View>
          <Text style={styles.name}>Nanang Hikmawan</Text>
          <Text style={styles.desc}>Prodact Manager</Text>
        </View>
        <View style={styles.footer}>
          <Button title="Unggah dan Lanjut" onPress={''} />
          <Gap height={30} />
          <Link label={'Lewati untuk ini'} align="center" size={16} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UploadPhoto;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    paddingHorizontal: 40,
    paddingBottom: 40,
    backgroundColor: colors.white,
    justifyContent: 'space-between',
  },
  image: {
    width: 110,
    height: 110,
  },
  header: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  main: {
    width: 130,
    height: 130,
    borderColor: colors.border,
    borderWidth: 1,
    borderRadius: 130 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    //
  },
  btn: {
    position: 'absolute',
    bottom: 8,
    right: 6,
    width: 30,
    height: 30,
  },
  name: {
    fontSize: 24,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
  },
  desc: {
    fontSize: 18,
    color: colors.text.secondary,
    fontFamily: fonts.primary[400],
    textAlign: 'center',
  },
  link: {
    color: colors.text.secondary,
    fontFamily: fonts.primary[400],
  },
});
