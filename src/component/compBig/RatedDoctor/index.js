import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors} from '../../../utils/colors';
import {DummyDoctor1, IconStar} from '../../../assets';
import {fonts} from '../../../utils/fonts';

const RatedDoctor = ({title, desc, type}) => {
  return (
    <View style={styles.container}>
      <Image source={DummyDoctor1} style={styles.img} />
      <View style={styles.wrapper}>
        <Text style={styles.text}>Alex Rachel</Text>
        <Text style={styles.text1}>Pediatrician</Text>
      </View>
      <View style={styles.star}>
        <IconStar />
        <IconStar />
        <IconStar />
        <IconStar />
        <IconStar />
        <IconStar />
      </View>
    </View>
  );
};

export default RatedDoctor;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 16,
  },
  img: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    marginRight: 12,
  },
  text: {
    fontFamily: fonts.primary[400],
    fontSize: 12,
    color: colors.text.primary,
  },
  text1: {
    fontFamily: fonts.primary[600],
    fontSize: 12,
    marginTop: 2,
    color: colors.text.secondary,
  },
  star: {
    flexDirection: 'row',
  },
  wrapper: {
    flex: 1,
  },
});
