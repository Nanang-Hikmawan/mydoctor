import {StyleSheet, Text, View, StatusBar, SafeAreaView} from 'react-native';
import React from 'react';
import {Header, List} from '../../component/compBig';
import {
  DummyDoctor10,
  DummyDoctor5,
  DummyDoctor6,
  DummyDoctor7,
  DummyDoctor9,
} from '../../assets';
import {colors} from '../../utils/colors';

const ChoseDoctor = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle="light-content"
      />
      <View style={styles.content}>
        <Header
          title={'Pilih Dokter Anak'}
          type="dark"
          onPress={() => navigation.goBack()}
        />
        <List
          profile={DummyDoctor7}
          name="Alexander Jannie"
          desc={'Wanita'}
          type="next"
          onPress={() => navigation.navigate('Chatting')}
        />
        <List
          profile={DummyDoctor6}
          name="John McParker Steve"
          desc={'Laki-laki'}
          type="next"
          onPress={() => navigation.navigate('Chatting')}
        />
        <List
          profile={DummyDoctor9}
          name="Nairobi Putri Hayza"
          desc={'Wanita'}
          type="next"
          onPress={() => navigation.navigate('Chatting')}
        />
        <List
          profile={DummyDoctor10}
          name="James Rivillia"
          desc={'Laki-laki'}
          type="next"
          onPress={() => navigation.navigate('Chatting')}
        />
        <List
          profile={DummyDoctor5}
          name="Liu Yue Tian Park"
          desc={'Wanita'}
          type="next"
          onPress={() => navigation.navigate('Chatting')}
        />
      </View>
    </SafeAreaView>
  );
};

export default ChoseDoctor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
