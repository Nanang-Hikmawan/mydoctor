import React, {useEffect} from 'react';
import {StyleSheet, Text, View, StatusBar, SafeAreaView} from 'react-native';
import {Logo} from '../../assets';
import {fonts} from '../../utils/fonts';
import {colors} from '../../utils/colors';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('GetStarted');
    }, 3000);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <View style={styles.content}>
        <Logo />
        <Text style={styles.text}>My Doctor</Text>
      </View>
    </SafeAreaView>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  text: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    marginTop: 20,
    color: colors.text.secondary,
  },
});
