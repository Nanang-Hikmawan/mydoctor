import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {fonts} from '../../../utils/fonts';
import {colors} from '../../../utils/colors';
import {DummyDoctor9} from '../../../assets';

const Other = () => {
  return (
    <View style={styles.content}>
      <Image source={DummyDoctor9} style={styles.img} />
      <View>
        <View style={styles.body}>
          <Text style={styles.text}>
            Oh tentu saja tidak karena jeruk itu sangat sehat...
          </Text>
        </View>
        <Text style={styles.text1}>4.45 AM</Text>
      </View>
    </View>
  );
};

export default Other;

const styles = StyleSheet.create({
  content: {
    marginBottom: 20,
    alignItems: 'flex-end',
    paddingLeft: 16,
    flexDirection: 'row',
  },
  img: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    marginRight: 12,
  },
  body: {
    maxWidth: '80%',
    padding: 12,
    paddingRight: 18,
    backgroundColor: colors.primary,
    borderRadius: 10,
    borderBottomLeftRadius: 0,
  },
  text: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: colors.white,
  },
  text1: {
    fontFamily: fonts.primary[400],
    fontSize: 11,
    color: colors.secondary,
    marginTop: 8,
  },
});
