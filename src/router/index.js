import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  GetStarted,
  Login,
  Register,
  SplashScreen,
  UploadPhoto,
  Doctors,
  Messages,
  Hospitals,
  ChoseDoctor,
  Chatting,
} from '../pages';
import {CustomTabBar} from '../component';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStack = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <CustomTabBar {...props} />}>
      <Tab.Screen name="Doctors" component={Doctors} />
      <Tab.Screen name="Messages" component={Messages} />
      <Tab.Screen name="Hospitals" component={Hospitals} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator
      initialRouteName="HomeStack"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="GetStarted" component={GetStarted} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="UplaodPhoto" component={UploadPhoto} />
      <Stack.Screen name="HomeStack" component={HomeStack} />
      <Stack.Screen name="ChoseDoctor" component={ChoseDoctor} />
      <Stack.Screen name="Chatting" component={Chatting} />
    </Stack.Navigator>
  );
};

export default Router;
