import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  Alert,
} from 'react-native';
import React from 'react';
import {Logo} from '../../assets';
import {Button, Gap, Input, Link} from '../../component';
import {fonts} from '../../utils/fonts';
import {colors} from '../../utils/colors';

const Login = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <View style={styles.content}>
        <Logo />
        <Text style={styles.title}>Masuk dan mulai berkonsultasi</Text>
        <Input label={'Alamat Email'} />
        <Gap height={24} />
        <Input label={'Password'} />
        <Gap height={10} />
        <Link
          label={'Lupa Password?'}
          size={12}
          onPress={() => Alert.alert('Lupa Password')}
        />
        <Button
          title={'Sign In'}
          onPress={() => navigation.replace('HomeStack')}
        />
        <Gap height={30} />
        <Link
          label={'Buat Akun Baru'}
          size={16}
          align="center"
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 40,
  },
  title: {
    color: colors.secondary,
    fontFamily: fonts.primary[600],
    fontSize: 20,
    marginVertical: 40,
    maxWidth: 153,
  },
});
