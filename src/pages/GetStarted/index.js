import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import {Background, Logo} from '../../assets';
import {Button, Gap} from '../../component';
import {fonts} from '../../utils/fonts';
import {colors} from '../../utils/colors';

const GetStarted = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Background} style={styles.content}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle={'light-content'}
        />
        <View>
          <Logo />
          <Text style={styles.title}>
            Konsultasi dengan dokter jadi lebih mudah & fleksibel
          </Text>
        </View>
        <View>
          <Button
            title={'Get Started'}
            onPress={() => navigation.navigate('Register')}
          />
          <Gap height={16} />
          <Button
            type={'secondary'}
            title={'Sign In'}
            onPress={() => navigation.navigate('Login')}
          />
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    padding: 40,
    justifyContent: 'space-between',
  },
  title: {
    color: colors.white,
    fontSize: 28,
    fontFamily: fonts.primary[600],
    marginTop: 91,
  },
});
